package com.sensorcon.wqm.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.sensorcon.sensordrone.android.tools.DroneStreamer;
import com.sensorcon.wqm.MainActivity;
import com.sensorcon.wqm.R;
import com.sensorcon.wqm.atlas.DOProbe;
import com.sensorcon.wqm.services.DroneService;
import com.sensorcon.wqm.ui.NUMPicker;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class DOMeterFragment extends BaseMeterFragment {

    DOProbe doProbe;
    DroneStreamer doStreamer;



    protected DOMeterFragment(DroneService service) {
        super(service);
    }

    public DOMeterFragment(DroneService service, DOProbe doProbe) {
        super(service);
        this.doProbe = doProbe;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        unitDisplay.setText("mg/L");
        display.setText("");
        setOffset("25","00000");

        doStreamer = new DroneStreamer(myService.serviceDrone, 1000) {
            @Override
            public void repeatableTask() {
                long time = Calendar.getInstance().getTimeInMillis();

                final String disOx = doProbe.doRead();
                // We want the percent DO saturation as well
                int commaPosition = disOx.indexOf(",");
                if (commaPosition == -1) {
                    doProbe.setPercentDoSaturationToggle();
                    return;
                }

                String doPercent = disOx.substring(0, commaPosition);
                final String doReading = disOx.substring(commaPosition+1, disOx.length());

                if (MainActivity.canLog) {
                    String toLog = String.valueOf(time) + "," + doPercent + "," + doReading + "\n";
                    try {
                        ((MainActivity)getActivity()).doOutputStream.write(toLog.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // We log percent, but only show mg/L reading
                        display.setText(doReading);
                    }
                });
            }


        };

        toggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!doStreamer.isRunning) {
                    doStreamer.start();
                    toggleBtn.setText("Stop");
                    setRunningStatus(true);
                    toggleBtn.setBackgroundResource(R.drawable.circle_off);
                }
                else {
                    doStreamer.stop();
                    toggleBtn.setText("Start");
                    setRunningStatus(false);
                    toggleBtn.setBackgroundResource(R.drawable.circle_on);
//                    String lastVal = (String)display.getText();
//                    lastVal += "\n(Not Running)";
//                    display.setText(lastVal);
                }
            }
        });


        sendDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.canLog) {
                    if (doStreamer.isRunning) {
                        MainActivity.uiToast(getActivity(),"Data can't be exported while measuring.");
                    }
                    else {
                        File toSend = ((MainActivity)getActivity()).doLog;
                        sendFile(toSend);
                    }
                }
                else {
                    MainActivity.genericDialog(DOMeterFragment.this.getActivity(),"Logging Disabled","The app was not able to access your device's external storage. Logging has been disabled!");
                }
            }
        });

    }

    public void offsetDialog() {
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Configure Compensation");
        builder.setMessage("Please select the Temperature and Conductivity of your sample:");

        // A View to hold it all
        ScrollView mainView = new ScrollView(getActivity());
        LinearLayout mainLayout = new LinearLayout(getActivity());
        mainLayout.setOrientation(LinearLayout.VERTICAL);
        mainView.addView(mainLayout);

        // Temperature
        TextView temp = new TextView(getActivity());
        temp.setText("Temperature (Celsius):");
        mainLayout.addView(temp);
        LinearLayout tempHolder = new LinearLayout(getActivity());
        tempHolder.setOrientation(LinearLayout.HORIZONTAL);
        tempHolder.setGravity(Gravity.CENTER);
        final NUMPicker tempChar1 = new NUMPicker(getActivity());
        final NUMPicker tempChar2 = new NUMPicker(getActivity());
        // Default is 25 C
        tempChar1.selectCharacter(2);
        tempChar2.selectCharacter(5);
        tempHolder.addView(tempChar1);
        tempHolder.addView(tempChar2);
        mainLayout.addView(tempHolder);

        // Conductivity
        TextView conductivity = new TextView(getActivity());
        conductivity.setText("Conductivity (Microsiemens):");
        mainLayout.addView(conductivity);
        LinearLayout conHolder = new LinearLayout(getActivity());
        conHolder.setOrientation(LinearLayout.HORIZONTAL);
        conHolder.setGravity(Gravity.CENTER);
        final NUMPicker conChar1 = new NUMPicker(getActivity());
        final NUMPicker conChar2 = new NUMPicker(getActivity());
        final NUMPicker conChar3 = new NUMPicker(getActivity());
        final NUMPicker conChar4 = new NUMPicker(getActivity());
        final NUMPicker conChar5 = new NUMPicker(getActivity());
        // Default is 0
        conHolder.addView(conChar1);
        conHolder.addView(conChar2);
        conHolder.addView(conChar3);
        conHolder.addView(conChar4);
        conHolder.addView(conChar5);
        mainLayout.addView(conHolder);
        TextView suggestion = new TextView(getActivity());
        suggestion.setText(
                "Some typical values are\n" +
                        "Fresh Water: 0\n" +
                        "Brackish Water: 19900\n" +
                        "Salt Water: 54000");
        mainLayout.addView(suggestion);

        // Put it all in the dialog
        builder.setView(mainView);

        builder.setIcon(R.drawable.ic_launcher);
        builder.setPositiveButton("Set Compensation", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Temperature
                String temp = "";
                temp += tempChar1.getSelectedCharacter();
                temp += tempChar2.getSelectedCharacter();
                // Conductivity
                String conductivity = "";
                conductivity += conChar1.getSelectedCharacter();
                conductivity += conChar2.getSelectedCharacter();
                conductivity += conChar3.getSelectedCharacter();
                conductivity += conChar4.getSelectedCharacter();
                conductivity += conChar5.getSelectedCharacter();

                // Adjust our read command from default
                String readCommand = temp + "," + conductivity;
                setOffset(temp,conductivity);




                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private void setOffset(String temp, String conductivity) {
        doProbe.setCompensation(temp + "," + conductivity + "\n");
        offsetDisplay.setText(temp + " \u00B0C\n" + conductivity + " \u00B5S");
        // Add a note in the log
        if (MainActivity.canLog) {
            String logInfo = "# Temperature/Conductivity compensation set to " + temp + " °C/" + conductivity + " \u00B5S\n";
            try {
                ((MainActivity)getActivity()).doOutputStream.write(logInfo.getBytes());
            } catch (IOException e) {
                //
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (doStreamer.isRunning) {
            doStreamer.stop();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.do_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        offsetDialog();
        return true;
    }
}
