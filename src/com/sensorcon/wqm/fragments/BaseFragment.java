package com.sensorcon.wqm.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import com.sensorcon.wqm.services.DroneService;

public abstract class BaseFragment extends Fragment {


    public SharedPreferences myPreferences;
    public SharedPreferences.Editor myEditor;
    public DroneService myService;

    protected BaseFragment(DroneService service) {
        this.myService = service;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        myEditor = myPreferences.edit();
    }


    public String parseInfo(String info) {
        int firstComma = info.indexOf(",");
        if (firstComma == -1 || firstComma == info.length()) {
            return "Error parsing Info";
        }
        int secondComma = info.indexOf(",", firstComma+1);
        String probeType = info.substring(0,firstComma);
        String fwVersion = info.substring(firstComma+1, secondComma);
        String fwDate = info.substring(secondComma+1, info.length());
        String result = "Probe Type: ";
        if (probeType.equals("P")) {
            result +="pH\n";
        }
        else if (probeType.equals("D")) {
            result += "Dissolved Oxygen\n";
        }
        else {
            result += "Unknown!";
        }
        result += "Firmware Version: " + fwVersion + "\n";
        result += "Firmware Date: "  + fwDate;
        return result;
    }
}
