package com.sensorcon.wqm.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import com.sensorcon.wqm.MainActivity;
import com.sensorcon.wqm.R;
import com.sensorcon.wqm.atlas.DOProbe;
import com.sensorcon.wqm.services.DroneService;
import com.sensorcon.wqm.ui.IDPicker;
import com.sensorcon.wqm.ui.TableRowWithIcon;

public class DOFragment extends BaseFragment{
    private TableLayout tableLayout;

    private DOProbe doProbe;

    protected DOFragment(DroneService service) {
        super(service);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.table_layout, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        doProbe = new DOProbe(myService);
        doProbe.setupProbe();

        tableLayout = (TableLayout)getActivity().findViewById(R.id.table_layout);

        TableRowWithIcon measureRow = new TableRowWithIcon(getActivity(), R.drawable.beaker_blue, "Measure Dissolved Oxygen");
        measureRow.setBgColor(R.drawable.blue_gradient);
        measureRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.loadFragment(getActivity(), new DOMeterFragment(myService, doProbe), true);
            }
        });

        TableRowWithIcon infoRow = new TableRowWithIcon(getActivity(), R.drawable.probe_info, "DO Probe Information");
        infoRow.setBgColor(R.drawable.blue_gradient);
        infoRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.genericDialog(getActivity(), "Probe Information", parseInfo(doProbe.getCircuitInfo()));
            }
        });

        // Do this automatically
//        TableRowWithIcon setupRow = new TableRowWithIcon(getActivity(), R.drawable.sd_settings, "Setup DO Probe");
//        setupRow.setBgColor(R.drawable.blue_gradient);
//        setupRow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                doProbe.setupProbe();
//                String msg = "This is only needed if you selected 'Dissolved Oxygen' from the main menu without the DO meter plugged in.";
//                MainActivity.genericDialog(getActivity(),"Probe Setup", msg);
//            }
//        });
        if (!MainActivity.DEBUG) {
            if (!doProbe.setupProbe()) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Probe not connected!");
                String warning =    "The DO probe is not connected to your Sensordrone. " +
                        "Please attach the probe and try again.";
                builder.setMessage(warning);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // Send the user back to the main menu
                        MainActivity.popMyBackStach(DOFragment.this.getActivity());
                        MainActivity.loadFragment(DOFragment.this.getActivity(), new MainFragment(myService), false);
                    }
                });
                dialog.show();
            }
        }

        TableRowWithIcon calRow = new TableRowWithIcon(getActivity(), R.drawable.beaker_blue, "Calibrate");
        calRow.setBgColor(R.drawable.blue_gradient);
        calRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Calibration");
                String warning = "";
                warning += "Dip your probe in water, just to get it wet. Let it sit in the air for 5 minutes.\n\n";
                warning += "Do not leave it in water, it must calibrate to the oxygen level in the surrounding atmosphere!\n\n";
                warning += "Calibration should be done the first time the D.O. circuit is used.\n\n";
                builder.setMessage(warning);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("It has been 5 minutes, Calibrate", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(), "Calibration Results", doProbe.calibrate());
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("I'm not ready, Cancel Calibration", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(), "Calibration Canceled", "No calibration was performed.");
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        });

        TableRowWithIcon setIDRow = new TableRowWithIcon(getActivity(), R.drawable.probe_set_id, "Set Probe Device ID");
        setIDRow.setBgColor(R.drawable.blue_gradient);
        setIDRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Set Probe ID");
                builder.setMessage("Please select a 4-Character ID\n(0-9, A-Z)");
                LinearLayout pickerHolder = new LinearLayout(getActivity());
                pickerHolder.setOrientation(LinearLayout.HORIZONTAL);
                pickerHolder.setGravity(Gravity.CENTER);
                final IDPicker char1 = new IDPicker(getActivity());
                final IDPicker char2 = new IDPicker(getActivity());
                final IDPicker char3 = new IDPicker(getActivity());
                final IDPicker char4 = new IDPicker(getActivity());
                pickerHolder.addView(char1);
                pickerHolder.addView(char2);
                pickerHolder.addView(char3);
                pickerHolder.addView(char4);
                builder.setView(pickerHolder);

                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("Set ID", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String id = "";
                        id += char1.getSelectedCharacter();
                        id += char2.getSelectedCharacter();
                        id += char3.getSelectedCharacter();
                        id += char4.getSelectedCharacter();
                        MainActivity.genericDialog(getActivity(), "Set ID", doProbe.setID(id));
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"Set ID", "ID not changed.");
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        });

        TableRowWithIcon readIDRow = new TableRowWithIcon(getActivity(), R.drawable.probe_query_id, "Query Probe Device ID");
        readIDRow.setBgColor(R.drawable.blue_gradient);
        readIDRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.genericDialog(getActivity(),"Probe ID", doProbe.queryID());
            }
        });

        TableRowWithIcon resetIDRow = new TableRowWithIcon(getActivity(), R.drawable.probe_reset_id, "Reset Probe Device ID");
        resetIDRow.setBgColor(R.drawable.blue_gradient);
        resetIDRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("ID Reset");
                String warning = "This will reset the previously assigned Probe ID.\n\nAre you sure you want to continue?";
                builder.setMessage(warning);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("Reset", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"ID Reset", doProbe.resetID());
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"ID Reset Canceled", "The ID has not been reset");
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        });

        TableRowWithIcon factoryResetRow = new TableRowWithIcon(getActivity(), R.drawable.probe_factory_reset, "Factory Reset");
        factoryResetRow.setBgColor(R.drawable.blue_gradient);
        factoryResetRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Factory Reset");
                String warning = "This will reset the DO probe circuit back to all factory default settings.\n\nAre you sure you want to continue?";
                builder.setMessage(warning);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("Reset", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(), "Factory Reset", doProbe.factoryReset());
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"Reset Canceled", "The settings have not been reset");
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        });


        tableLayout.addView(measureRow);
        tableLayout.addView(infoRow);
//        tableLayout.addView(setupRow);
        tableLayout.addView(calRow);
        tableLayout.addView(setIDRow);
        tableLayout.addView(readIDRow);
        tableLayout.addView(resetIDRow);
        tableLayout.addView(factoryResetRow);
    }

}
