package com.sensorcon.wqm.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import com.sensorcon.wqm.MainActivity;
import com.sensorcon.wqm.services.DroneService;
import com.sensorcon.wqm.R;
import com.sensorcon.wqm.ui.TableRowWithIcon;

import java.io.File;

public class SettingsFragment extends BaseFragment {
    protected SettingsFragment(DroneService service) {
        super(service);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.table_layout, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TableLayout tableLayout = (TableLayout) getActivity().findViewById(R.id.table_layout);

        TableRowWithIcon phLogRow = new TableRowWithIcon(getActivity(), R.drawable.beaker_ph, "Send pH Log File");
        phLogRow.setBgColor(R.drawable.green_gradient);
        phLogRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.canLog) {
                    sendFile(((MainActivity)getActivity()).phLog);
                }
                else {
                    MainActivity.genericDialog(SettingsFragment.this.getActivity(),"Logging Disabled","The app was not able to access your device's external storage. Logging has been disabled!");
                }
            }
        });

        TableRowWithIcon doLogRow = new TableRowWithIcon(getActivity(), R.drawable.beaker_do, "Send DO Log File");
        doLogRow.setBgColor(R.drawable.green_gradient);
        doLogRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.canLog) {
                    sendFile(((MainActivity) getActivity()).doLog);
                }
                else {
                    MainActivity.genericDialog(SettingsFragment.this.getActivity(),"Logging Disabled","The app was not able to access your device's external storage. Logging has been disabled!");
                }
            }
        });

        tableLayout.addView(phLogRow);
        tableLayout.addView(doLogRow);


    }

    public void sendFile(File dataLog) {
        Uri fileUri = Uri.fromFile(dataLog);
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Sensordrone Water Quality Data");
        sendIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        sendIntent.setType("text/html");
        startActivity(sendIntent);
    }
}
