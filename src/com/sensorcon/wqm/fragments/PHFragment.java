package com.sensorcon.wqm.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import com.sensorcon.wqm.MainActivity;
import com.sensorcon.wqm.R;
import com.sensorcon.wqm.atlas.PHProbe;
import com.sensorcon.wqm.services.DroneService;
import com.sensorcon.wqm.ui.IDPicker;
import com.sensorcon.wqm.ui.TableRowWithIcon;

public class PHFragment extends BaseFragment{
    private TableLayout tableLayout;

    private PHProbe phProbe;
    private final String TAG = "WQM:PHFragment";

    public PHFragment(DroneService service) {
        super(service);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.table_layout, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        phProbe = new PHProbe(myService);
        phProbe.setUpProbe();

        tableLayout = (TableLayout)getActivity().findViewById(R.id.table_layout);

        TableRowWithIcon measureRow = new TableRowWithIcon(getActivity(), R.drawable.ic_launcher, "Measure pH");
        measureRow.setBgColor(R.drawable.red_gradient);
        measureRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.loadFragment(getActivity(), new PHMeterFragment(myService, phProbe), true);
            }
        });

        TableRowWithIcon infoRow = new TableRowWithIcon(getActivity(), R.drawable.probe_info, "pH Probe Information");
        infoRow.setBgColor(R.drawable.red_gradient);
        infoRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.genericDialog(getActivity(),"Probe Information", parseInfo(phProbe.getCircuitInfo()));
            }
        });

        // Try and do this automatically
//        TableRowWithIcon setupRow = new TableRowWithIcon(getActivity(), R.drawable.sd_settings, "Setup pH Probe");
//        setupRow.setBgColor(R.drawable.red_gradient);
//        setupRow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                phProbe.setUpProbe();
//                String msg = "This is only needed if you selected 'pH' from the main menu without the pH meter plugged in.";
//                MainActivity.genericDialog(getActivity(),"Probe Setup", msg);
//            }
//        });
        if (!MainActivity.DEBUG) {
            if (!phProbe.setUpProbe()) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Probe not connected!");
                String warning =    "The pH probe is not connected to your Sensordrone. " +
                        "Please attach the probe and try again.";
                builder.setMessage(warning);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // Send the user back to the main menu
                        MainActivity.popMyBackStach(PHFragment.this.getActivity());
                        MainActivity.loadFragment(PHFragment.this.getActivity(), new MainFragment(myService), false);
                    }
                });
                dialog.show();
            }
        }


        TableRowWithIcon cal4Row = new TableRowWithIcon(getActivity(), R.drawable.beaker_cal_4, "Calibrate at pH 4");
        cal4Row.setBgColor(R.drawable.red_gradient);
        cal4Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("pH 4 Calibration");
                String warning = "";
                warning += "Please immerse your pH probe into a solution with a pH of 4.";
                warning += "\n\nWhen ready to store the calibration data, press 'Calibrate'.";
                warning += "\n\nCalibrating when the pH sensor is not immersed in a pH 4 solution will calibrate the pH Circuit to see that whatever it was reading to now be a pH 4 and could lead to significant errors.";
                builder.setMessage(warning);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("Calibrate", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(), "Calibration Results", phProbe.calibrate4());
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancel Calibration", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(), "Calibration Canceled", "No calibration was performed.");
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        });

        TableRowWithIcon cal7Row = new TableRowWithIcon(getActivity(), R.drawable.beaker_cal_7, "Calibrate at pH 7");
        cal7Row.setBgColor(R.drawable.red_gradient);
        cal7Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("pH 7 Calibration");
                String warning = "";
                warning += "Please immerse your pH probe into a solution with a pH of 7.";
                warning += "\n\nWhen ready to store the calibration data, press 'Calibrate'.";
                warning += "\n\nCalibrating when the pH sensor is not immersed in a pH 7 solution will calibrate the pH Circuit to see that whatever it was reading to now be a pH 7 and could lead to significant errors.";
                builder.setMessage(warning);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("Calibrate", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"Calibration Results", phProbe.calibrate7());
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancel Calibration", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"Calibration Canceled", "No calibration was performed.");
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        });

        TableRowWithIcon cal10Row = new TableRowWithIcon(getActivity(), R.drawable.beaker_cal_10, "Calibrate at pH 10");
        cal10Row.setBgColor(R.drawable.red_gradient);
        cal10Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("pH 10 Calibration");
                String warning = "";
                warning += "Please immerse your pH probe into a solution with a pH of 10.";
                warning += "\n\nWhen ready to store the calibration data, press 'Calibrate'.";
                warning += "\n\nCalibrating when the pH sensor is not immersed in a pH 10 solution will calibrate the pH Circuit to see that whatever it was reading to now be a pH 10 and could lead to significant errors.";
                builder.setMessage(warning);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("Calibrate", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"Calibration Results", phProbe.calibrate10());
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancel Calibration", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"Calibration Canceled", "No calibration was performed.");
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        });

        TableRowWithIcon setIDRow = new TableRowWithIcon(getActivity(), R.drawable.probe_set_id, "Set pH Probe Device ID");
        setIDRow.setBgColor(R.drawable.red_gradient);
        setIDRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Set Probe ID");
                builder.setMessage("Please select a 4-Character ID\n(0-9, A-Z)");
                LinearLayout pickerHolder = new LinearLayout(getActivity());
                pickerHolder.setOrientation(LinearLayout.HORIZONTAL);
                pickerHolder.setGravity(Gravity.CENTER);
                final IDPicker char1 = new IDPicker(getActivity());
                final IDPicker char2 = new IDPicker(getActivity());
                final IDPicker char3 = new IDPicker(getActivity());
                final IDPicker char4 = new IDPicker(getActivity());
                pickerHolder.addView(char1);
                pickerHolder.addView(char2);
                pickerHolder.addView(char3);
                pickerHolder.addView(char4);
                builder.setView(pickerHolder);

                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("Set ID", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String id = "";
                        id += char1.getSelectedCharacter();
                        id += char2.getSelectedCharacter();
                        id += char3.getSelectedCharacter();
                        id += char4.getSelectedCharacter();
                        MainActivity.genericDialog(getActivity(), "Set ID", phProbe.setID(id));
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"Set ID", "ID not changed.");
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        });

        TableRowWithIcon readIDRow = new TableRowWithIcon(getActivity(), R.drawable.probe_query_id, "Query Probe Device ID");
        readIDRow.setBgColor(R.drawable.red_gradient);
        readIDRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.genericDialog(getActivity(),"pH Probe ID", phProbe.queryID());
            }
        });

        TableRowWithIcon resetIDRow = new TableRowWithIcon(getActivity(), R.drawable.probe_reset_id, "Reset Probe Device ID");
        resetIDRow.setBgColor(R.drawable.red_gradient);
        resetIDRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("ID Reset");
                String warning = "This will reset the previously assigned pH probe ID.\n\nAre you sure you want to continue?";
                builder.setMessage(warning);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("Reset", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"ID Reset", phProbe.resetID());
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"ID Reset Canceled", "The ID has not been reset");
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        });

        // We don't need the Debug LED options, since the board will be in a housing that covers them up.
        // If you want the option, uncomment these

//        TableRowWithIcon debugOnRow = new TableRowWithIcon(getActivity(), R.drawable.ic_launcher, "Set Debug LEDs ON");
//        debugOnRow.setBgColor(R.drawable.red_gradient);
//        debugOnRow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                doProbe.enableDebugLEDs();
//                String msg = "Debugging LEDs are now set to ON.\n\n";
//                msg += "This setting is saved on the pH probe circuit, and will remain in effect even if the power is disconnected.\n\n";
//                msg += "The GREEN LED indicates data sent over the TX.\n\n";
//                msg += "The RED LED indicates that the pH probe was sent an unrecognized command";
//                MainActivity.genericDialog(getActivity(),"Debugging LEDS ON",msg);
//            }
//        });
//
//        TableRowWithIcon debugOffRow = new TableRowWithIcon(getActivity(), R.drawable.ic_launcher, "Set Debug LEDs OFF");
//        debugOffRow.setBgColor(R.drawable.red_gradient);
//        debugOffRow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                doProbe.disableDebugLEDs();
//                String msg = "Debugging LEDs are now set to OFF.\n\n";
//                msg += "This setting is saved on the pH probe circuit, and will remain in effect even if the power is disconnected.\n\n";
//                MainActivity.genericDialog(getActivity(),"Debugging LEDS OFF",msg);
//            }
//        });

        TableRowWithIcon factoryResetRow = new TableRowWithIcon(getActivity(), R.drawable.probe_factory_reset, "Factory Reset");
        factoryResetRow.setBgColor(R.drawable.red_gradient);
        factoryResetRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Factory Reset");
                String warning = "This will reset the pH probe circuit back to all factory default settings.\n\nAre you sure you want to continue?";
                builder.setMessage(warning);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("Reset", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"Factory Reset", phProbe.factoryReset());
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.genericDialog(getActivity(),"Reset Canceled", "The settings have not been reset");
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        });


        tableLayout.addView(measureRow);
        tableLayout.addView(infoRow);
//        tableLayout.addView(setupRow);
        tableLayout.addView(cal4Row);
        tableLayout.addView(cal7Row);
        tableLayout.addView(cal10Row);
        tableLayout.addView(setIDRow);
        tableLayout.addView(readIDRow);
        tableLayout.addView(resetIDRow);
        // See above to uncomment
//        tableLayout.addView(debugOnRow);
//        tableLayout.addView(debugOffRow);
        tableLayout.addView(factoryResetRow);
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,"onPause");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy");

    }

}
