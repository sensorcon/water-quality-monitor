package com.sensorcon.wqm.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.sensorcon.sensordrone.android.tools.DroneStreamer;
import com.sensorcon.wqm.MainActivity;
import com.sensorcon.wqm.R;
import com.sensorcon.wqm.atlas.PHProbe;
import com.sensorcon.wqm.services.DroneService;
import com.sensorcon.wqm.ui.NUMPicker;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class PHMeterFragment extends BaseMeterFragment {

    PHProbe phProbe;
    DroneStreamer phStreamer;

    ArrayList<Long> measureTime;
    ArrayList<Float> measurePH;

    private final String TAG = "WQM:PHMeterFragment";

    PowerManager powerManager;
    PowerManager.WakeLock screenWakeLock;


    protected PHMeterFragment(DroneService service) {
        super(service);
    }

    public PHMeterFragment(DroneService service, PHProbe phProbe) {
        super(service);
        this.phProbe = phProbe;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        unitDisplay.setText("pH");
        display.setText("");

        setOffset("25");

        measureTime = new ArrayList<Long>();
        measurePH = new ArrayList<Float>();

        phStreamer = new DroneStreamer(myService.serviceDrone, 1000) {
            @Override
            public void repeatableTask() {
                long time = Calendar.getInstance().getTimeInMillis();

                final String ph = phProbe.phRead();
                float phReading;
                try {
                    phReading = Float.parseFloat(ph);
                    measurePH.add(phReading);
                    measureTime.add(time);
                    if (MainActivity.canLog) {
                        String toLog = String.valueOf(time) + "," + phReading + "\n";
                        try {
                            ((MainActivity)getActivity()).phOutputStream.write(toLog.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                catch (NumberFormatException nfe) {
                    //
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        display.setText(ph);
                    }
                });
            }


        };

        toggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!phStreamer.isRunning) {
                    phStreamer.start();
                    toggleBtn.setText("Stop");
                    setRunningStatus(true);
                    toggleBtn.setBackgroundResource(R.drawable.circle_off);
                }
                else {
                    phStreamer.stop();
                    toggleBtn.setText("Start");
                    setRunningStatus(false);
                    toggleBtn.setBackgroundResource(R.drawable.circle_on);
//                    String lastVal = (String)display.getText();
//                    lastVal += "\n(Not Running)";
//                    display.setText(lastVal);
                }
            }
        });


        sendDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.canLog) {
                    if (phStreamer.isRunning) {
                        MainActivity.uiToast(getActivity(),"Data can't be exported while measuring.");
                    }
                    else {
                        File toSend = ((MainActivity)getActivity()).phLog;
                        sendFile(toSend);
                    }
                }
                else {
                    MainActivity.genericDialog(PHMeterFragment.this.getActivity(),"Logging Disabled","The app was not able to access your device's external storage. Logging has been disabled!");
                }
            }

        });


    }

    public void offsetDialog() {
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Configure Compensation");
        builder.setMessage("Please select the Temperature of your sample:");

        // A View to hold it all
        ScrollView mainView = new ScrollView(getActivity());
        LinearLayout mainLayout = new LinearLayout(getActivity());
        mainLayout.setOrientation(LinearLayout.VERTICAL);
        mainView.addView(mainLayout);

        // Temperature
        TextView temp = new TextView(getActivity());
        temp.setText("Temperature (Celsius):");
        mainLayout.addView(temp);
        LinearLayout tempHolder = new LinearLayout(getActivity());
        tempHolder.setOrientation(LinearLayout.HORIZONTAL);
        tempHolder.setGravity(Gravity.CENTER);
        final NUMPicker tempChar1 = new NUMPicker(getActivity());
        final NUMPicker tempChar2 = new NUMPicker(getActivity());
        // Default is 25 C
        tempChar1.selectCharacter(2);
        tempChar2.selectCharacter(5);
        tempHolder.addView(tempChar1);
        tempHolder.addView(tempChar2);
        mainLayout.addView(tempHolder);

        // Put it all in the dialog
        builder.setView(mainView);

        builder.setIcon(R.drawable.ic_launcher);
        builder.setPositiveButton("Set Compensation", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Temperature
                String temp = "";
                temp += tempChar1.getSelectedCharacter();
                temp += tempChar2.getSelectedCharacter();

                setOffset(temp);



                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private void setOffset(String offset) {
        phProbe.setCompensation(offset+"\n");
        offsetDisplay.setText(offset + " \u00B0C");
//        Add a note in the log
        if (MainActivity.canLog) {
            String logInfo = "# Temperature compensation set to " + offset + " \u00B0C\n";
            try {
                ((MainActivity)getActivity()).phOutputStream.write(logInfo.getBytes());
            } catch (IOException e) {
                //
            }
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (phStreamer.isRunning) {
            phStreamer.stop();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.ph_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        offsetDialog();
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,"onPause");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy");

    }
}
