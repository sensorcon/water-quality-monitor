package com.sensorcon.wqm.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.Toast;
import com.sensorcon.wqm.MainActivity;
import com.sensorcon.wqm.R;
import com.sensorcon.wqm.services.DroneService;
import com.sensorcon.wqm.ui.TableRowWithIcon;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;
import com.sensorcon.sensordrone.android.tools.DroneConnectionHelper;

public class MainFragment extends BaseFragment implements DroneEventHandler {


    TableLayout tableLayout;
    TableRowWithIcon connectRow;

    private String CONNECTED = "Connected\n(Click to Disconnect)";
    private String DISCONNECTED = "Disconnected\n(Click to Connect)";


    public MainFragment(DroneService service) {
        super(service);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.table_layout, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        MainActivity.popMyBackStach(this.getActivity());

        tableLayout = (TableLayout)getActivity().findViewById(R.id.table_layout);

        TableRowWithIcon phRow = new TableRowWithIcon(getActivity(), R.drawable.beaker_ph, "pH");
        phRow.setBgColor(R.drawable.red_gradient);
        phRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myService.serviceDrone.isConnected || MainActivity.DEBUG) {
                    MainActivity.loadFragment(getActivity(), new PHFragment(myService), true);
                }
                else {
                    MainActivity.genericDialog(getActivity(), "Not Connected!", "Please connect to your Sensordrone before trying to measure pH.");
                }
            }
        });

        TableRowWithIcon doRow = new TableRowWithIcon(getActivity(), R.drawable.beaker_do, "Dissolved Oxygen");
        doRow.setBgColor(R.drawable.blue_gradient);
        doRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myService.serviceDrone.isConnected || MainActivity.DEBUG) {
                    MainActivity.loadFragment(getActivity(), new DOFragment(myService), true);
                }
                else {
                    MainActivity.genericDialog(getActivity(), "Not Connected!", "Please connect to your Sensordrone before trying to measure DO.");
                }
            }
        });
        // Check if we are already connected or not
        if (myService.serviceDrone.isConnected) {
            connectRow = new TableRowWithIcon(getActivity(), R.drawable.sd_connected, CONNECTED);
        }
        else {
            connectRow = new TableRowWithIcon(getActivity(), R.drawable.sd_not_connected, DISCONNECTED);
        }
        connectRow.setBgColor(R.drawable.purple_gradient);
        connectRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myService.serviceDrone.isConnected) {
                    myService.serviceDrone.disconnect();
                }
                else {
                    DroneConnectionHelper helper = new DroneConnectionHelper();
                    helper.connectFromPairedDevices(myService.serviceDrone, getActivity());
                }
            }
        });

        TableRowWithIcon settingsRow = new TableRowWithIcon(getActivity(), R.drawable.sd_settings, "App Settings");
        settingsRow.setBgColor(R.drawable.green_gradient);
        settingsRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.loadFragment(getActivity(), new SettingsFragment(myService), true);
            }
        });



        tableLayout.addView(phRow);
        tableLayout.addView(doRow);
        tableLayout.addView(connectRow);
        tableLayout.addView(settingsRow);




    }

    @Override
    public void onResume() {
        super.onResume();
        myService.serviceDrone.registerDroneListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        myService.serviceDrone.unregisterDroneListener(this);
    }


    public void uiToast(final String msg) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        if (droneEventObject.matches(DroneEventObject.droneEventType.CONNECTED)) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    connectRow.setIconResource(R.drawable.sd_connected);
                    connectRow.setDescription("Connected!\n(click to disconnect)");
                    // The probe's default baudrates are 38400
                    myService.serviceDrone.setBaudRate_38400();
                }
            });
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.DISCONNECTED)) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    connectRow.setIconResource(R.drawable.sd_not_connected);
                    connectRow.setDescription("Disconnected!\n(click to connect)");
                }
            });
        }

    }

}
