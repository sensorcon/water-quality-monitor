package com.sensorcon.wqm.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.sensorcon.wqm.R;
import com.sensorcon.wqm.services.DroneService;

import java.io.File;

public class BaseMeterFragment extends BaseFragment{

    TextView sendDataBtn;
    TextView toggleBtn;
    TextView display;
    TextView unitDisplay;
    TextView offsetDisplay;
    TextView runningStatus;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.meter, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sendDataBtn = (TextView)getActivity().findViewById(R.id.meter_btn_send);
        toggleBtn = (TextView)getActivity().findViewById(R.id.meter_tv_toggle);
        display = (TextView)getActivity().findViewById(R.id.meter_tv_ph);
        unitDisplay = (TextView)getActivity().findViewById(R.id.meter_tv_unit);
        offsetDisplay = (TextView)getActivity().findViewById(R.id.meter_tv_offset);
        runningStatus = (TextView)getActivity().findViewById(R.id.meter_tv_runningstatus);
        setRunningStatus(false);


    }

    protected BaseMeterFragment(DroneService service) {
        super(service);
    }

    public void setRunningStatus(boolean isRunning) {
        if (isRunning) {
            runningStatus.setVisibility(View.INVISIBLE);
        }
        else {
            runningStatus.setVisibility(View.VISIBLE);
        }
    }

    public void sendFile(File dataLog) {
        Uri fileUri = Uri.fromFile(dataLog);
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Sensordrone Water Quality Data");
        sendIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        sendIntent.setType("text/html");
        startActivity(sendIntent);
    }

}
