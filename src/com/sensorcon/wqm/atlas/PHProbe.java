package com.sensorcon.wqm.atlas;

import android.util.Log;
import com.sensorcon.wqm.services.DroneService;

import java.nio.ByteBuffer;

/**
 * A class to interact with an Atlas Scientific pH Probe
 *
 * Required pH hardware:
 * pH Circuit version 5.0 firmware v5.0
 *
 */
public class PHProbe {

    private DroneService phService;
    private String TAG = "PHProbe";

    public PHProbe(DroneService phService) {
        this.phService = phService;
    }

    // These are the commands for the Atlas Scientific pHProbe
    // Hardware v5.0
    // Firmware v5.0
    // As shown in http://atlas-scientific.com/_files/pH_Circuit_5.0.pdf
    static final byte[] ENABLE_DEBUG_LEDS = "L1\r".getBytes();
    static final byte[] DISABLE_DEBUG_LEDS = "L0\r".getBytes();
    static final byte[] TAKE_SINGLE_READING = "R\r".getBytes();
    static final byte[] SET_CONTINUOUS_READINGS = "C\r".getBytes();
    static final byte[] STANDBY_MODE = "E\r".getBytes();
    static final byte[] CALIBRATE_PH_7 = "S\r".getBytes();
    static final byte[] CALIBRATE_PH_4 = "F\r".getBytes();
    static final byte[] CALIBRATE_PH_10 = "T\r".getBytes();
    static final byte[] FACTORY_RESET = "X\r".getBytes();
    static final byte[] INFORMATION = "I\r".getBytes();
    static final byte[] RESET_ID = "#!\r".getBytes();
    static final byte[] QUERY_ID = "#?\r".getBytes();

    /**
     * This will enable both Debugging LEDs. This settings is written to EEPROM
     * and will be remembered between power cycles.
     */
    public void enableDebugLEDs() {
        phService.serviceDrone.uartWrite(ENABLE_DEBUG_LEDS);
    }

    /**
     * This will disable both Debugging LEDs. This settings is written to EEPROM
     * and will be remembered between power cycles.
     */
    public void disableDebugLEDs() {
        phService.serviceDrone.uartWrite(DISABLE_DEBUG_LEDS);
    }

    public void setCompensation(String compensation) {
        phService.serviceDrone.uartWrite(compensation.getBytes());
    }

    public String phRead() {
        byte[] phReading = phService.serviceDrone.uartWriteForRead(TAKE_SINGLE_READING, 400);
        if (phReading.length == 1) {
            return "Communication Problem";
        }
        String result ="";
        for (int i = 0; i < phReading.length; i++) {
            // ASCII spaces and higher
            if ((int)phReading[i] > 31) {
                byte[] aChar = {phReading[i]};
                result += new String(aChar);
            }
        }

        return new String(result);
    }

    public String getCircuitInfo() {
        byte[] info = phService.serviceDrone.uartWriteForRead(INFORMATION, 100);
        return byteToString(info);
    }

    public void setToStandby() {
        phService.serviceDrone.uartWrite(STANDBY_MODE);
        // Do two reads, just to clear out the buffer in case
        // NOTE: this will trigger the uartRead in any listeners!
        phService.serviceDrone.uartRead();
        phService.serviceDrone.uartRead();
    }

    public boolean setUpProbe() {
        // Send the commands
        setToStandby();
        disableDebugLEDs();
        // Read the circuit Info to make sure the Probe is plugged in
        String info = getCircuitInfo();
        int firstComma = info.indexOf(",");
        if (firstComma == -1 || firstComma == info.length()) {
            // No comma means no response, means probe not plugged in.
            return false;
        }
        String probeType = info.substring(0,firstComma);
        if (probeType.equals("P")) {
            setToStandby();
            disableDebugLEDs();
            return true;
        }

        // If it didn't return "P" then it's not a pH probe.
        return false;
    }


    public String calibrate4() {
        byte[] calResult = phService.serviceDrone.uartWriteForRead(CALIBRATE_PH_4, 100);
        if (calResult.length == 1) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying calibration";
        }
        // The calibration doesn't seem to work the way the documentation says.
        // It doesn't seem to respond with a calibrated reading...
        // So we take one ourselves

        String response = phRead();
        try {
            float reading = Float.parseFloat(response);
            if (reading < 4.1 && reading > 3.9) {
                return "Calibration Successful!";
            }
            else {
                return "Calibration reading is " + reading + "\n\nRe-run calibration to try and get a more accurate calibration.";
            }
        } catch (NumberFormatException nfe) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying calibration";
        }

    }

    public String calibrate7() {
        byte[] calResult = phService.serviceDrone.uartWriteForRead(CALIBRATE_PH_7, 500);
        if (calResult.length == 1) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying calibration";
        }
        // The calibration doesn't seem to work the way the documentation says.
        // It doesn't seem to respond with a calibrated reading...
        // So we take one ourselves

        String response = phRead();
        try {
            float reading = Float.parseFloat(response);
            if (reading < 7.1 && reading > 6.9) {
                return "Calibration Successful!";
            }
            else {
                return "Calibration reading is " + reading + "\n\nRe-run calibration to try and get a more accurate calibration.";
            }
        } catch (NumberFormatException nfe) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying calibration";
        }
    }

    public String calibrate10() {
        byte[] calResult = phService.serviceDrone.uartWriteForRead(CALIBRATE_PH_10, 500);
        if (calResult.length == 1) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying calibration";
        }
        // The calibration doesn't seem to work the way the documentation says.
        // It doesn't seem to respond with a calibrated reading...
        // So we take one ourselves

        String response = phRead();
        try {
            float reading = Float.parseFloat(response);
            if (reading < 10.1 && reading > 9.9) {
                return "Calibration Successful!";
            }
            else {
                return "Calibration Value is " + reading + "\n\nRe-run calibration to try and get a more accurate calibration.";
            }
        } catch (NumberFormatException nfe) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying calibration";
        }
    }

    private String byteToString(byte[] phReading) {
        if (phReading.length == 1) {
            return "Communication Problem";
        }
        // Strip out non ascii charcters
        ByteBuffer buffer = ByteBuffer.allocate(32);
        for (int i = 0; i < phReading.length; i++) {
            if ((int)phReading[i] < 128) {
                buffer.put(phReading[i]);
            }
        }

        return new String(buffer.array());
    }

    public String factoryReset() {
        byte[] response = phService.serviceDrone.uartWriteForRead(FACTORY_RESET, 500);
        if (response.length == 1) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying";
        }
        // Strip out non ascii characters
        ByteBuffer buffer = ByteBuffer.allocate(32);
        for (int i = 0; i < response.length; i++) {
            if ((int)response[i] < 128) {
                buffer.put(response[i]);
            }
        }
        String resetResult = new String(buffer.array());
        Log.d(TAG, "Factory reset response: " + resetResult);
        if (resetResult.contains("reset")) {
            setUpProbe();
            return "The probe circuit has been reset to factory defaults";
        }
        else {
            return "Unexpected response from pH probe.\n\nPlease try again!";
        }
    }

    public String setID(String id) {
        String command = "#" + id + "\r";
        byte[] response = phService.serviceDrone.uartWriteForRead(command.getBytes(), 300);
        ByteBuffer buffer = ByteBuffer.allocate(32);
        for (int i = 0; i < response.length; i++) {
            if ((int)response[i] < 128) {
                buffer.put(response[i]);
            }
        }
        String result = new String(buffer.array());
        if (result.contains("Set")) {
            return "The probe ID has been set";
        }
        else {
            return "Unexpected response from pH probe.\n\nPlease try again!";
        }

    }
    public String resetID() {
        byte[] response = phService.serviceDrone.uartWriteForRead(RESET_ID, 100);
        if (response.length == 1) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying";
        }
        // Strip out non ascii characters
        ByteBuffer buffer = ByteBuffer.allocate(32);
        for (int i = 0; i < response.length; i++) {
            if ((int)response[i] < 128) {
                buffer.put(response[i]);
            }
        }
        String result = new String(buffer.array());
        Log.d(TAG, "Reset ID response: " + result);
        if (result.contains("clr")) {
            return "The probe circuit ID has been reset";
        }
        else {
            return "Unexpected response from pH probe.\n\nPlease try again!";
        }

    }

    public String queryID() {
        byte[] response = phService.serviceDrone.uartWriteForRead(QUERY_ID, 100);
        if (response.length == 1) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying";
        }
        // Strip out non ascii characters
        ByteBuffer buffer = ByteBuffer.allocate(32);
        for (int i = 0; i < response.length; i++) {
            if ((int)response[i] < 128) {
                buffer.put(response[i]);
            }
        }
        String result = new String(buffer.array());
        Log.d(TAG, "Query ID response: " + result);
        return result;
    }


}
