package com.sensorcon.wqm.atlas;

import android.util.Log;
import com.sensorcon.wqm.services.DroneService;

import java.nio.ByteBuffer;

/**
 * A class to interact with an Atlas Scientific Dissolved Oxygen Probe
 *
 * Required DO hardware:
 * DO Circuit version 5.0 firmware v5
 *
 */
public class DOProbe {

    private DroneService phService;
    private String TAG = "DOProbe";

    public DOProbe(DroneService phService) {
        this.phService = phService;
    }

    // These are the commands for the Atlas Scientific pHProbe
    // Hardware v5.0
    // Firmware v5.0
    // As shown in http://atlas-scientific.com/_files/pH_Circuit_5.0.pdf
    static final byte[] ENABLE_DEBUG_LEDS = "L1\r".getBytes();
    static final byte[] DISABLE_DEBUG_LEDS = "L0\r".getBytes();
    static final byte[] TAKE_SINGLE_READING = "R\r".getBytes();
//    static final byte[] SET_CONTINUOUS_READINGS = "C\r".getBytes();
    static final byte[] PERCENT_DO_SATURATION_TOGGLE = "%\r".getBytes();
    static final byte[] STANDBY_MODE = "E\r".getBytes();

    static final byte[] CALIBRATE= "M\r".getBytes();
    static final byte[] FACTORY_RESET = "X\r".getBytes();
    static final byte[] INFORMATION = "I\r".getBytes();
    static final byte[] RESET_ID = "#!\r".getBytes();
    static final byte[] QUERY_ID = "#?\r".getBytes();


    public void setREAD_COMMAND(byte[] READ_COMMAND) {
        this.READ_COMMAND = READ_COMMAND;
    }

    private byte[] READ_COMMAND = TAKE_SINGLE_READING;
    /**
     * This will enable both Debugging LEDs. This settings is written to EEPROM
     * and will be remembered between power cycles.
     */
    public void enableDebugLEDs() {
        phService.serviceDrone.uartWrite(ENABLE_DEBUG_LEDS);
    }

    /**
     * This will disable both Debugging LEDs. This settings is written to EEPROM
     * and will be remembered between power cycles.
     */
    public void disableDebugLEDs() {
        phService.serviceDrone.uartWrite(DISABLE_DEBUG_LEDS);
    }

    public String doRead() {
        byte[] doReading = phService.serviceDrone.uartWriteForRead(READ_COMMAND, 400);
        if (doReading.length == 1) {
            return "Communication Problem";
        }
        // Strip out non ascii characters
        String result ="";
        for (int i = 0; i < doReading.length; i++) {
            // ASCII spaces and higher
            if ((int)doReading[i] > 31) {
                byte[] aChar = {doReading[i]};
                result += new String(aChar);
            }
        }

        Log.d("PING", result);

        return result;
    }

    public String getCircuitInfo() {
        byte[] info = phService.serviceDrone.uartWriteForRead(INFORMATION, 100);
        return byteToString(info);
    }

    public void setToStandby() {
        phService.serviceDrone.uartWrite(STANDBY_MODE);
        // Do two reads, just to clear out the buffer in case
        // NOTE: this will trigger the uartRead in any listeners!
        phService.serviceDrone.uartRead();
        phService.serviceDrone.uartRead();
    }

    public boolean setupProbe() {
        setToStandby();
        disableDebugLEDs();
        setPercentDoSaturationToggle();
        // Read the circuit Info to make sure the Probe is plugged in
        String info = getCircuitInfo();
        int firstComma = info.indexOf(",");
        if (firstComma == -1 || firstComma == info.length()) {
            // No comma means no response, means probe not plugged in.
            return false;
        }
        String probeType = info.substring(0,firstComma);
        if (probeType.equals("D")) {
            setToStandby();
            disableDebugLEDs();
            setPercentDoSaturationToggle();
            return true;
        }

        // If it didn't return "D" then it's not a DO probe.
        return false;
    }

    public void setPercentDoSaturationToggle() {
        phService.serviceDrone.uartWrite(PERCENT_DO_SATURATION_TOGGLE);
    }

    public String calibrate() {
        byte[] calResult = phService.serviceDrone.uartWriteForRead(CALIBRATE, 500);
        if (calResult.length == 1) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying calibration";
        }
        // Strip out non ascii characters
        ByteBuffer buffer = ByteBuffer.allocate(32);
        for (int i = 0; i < calResult.length; i++) {
            if ((int)calResult[i] < 128) {
                buffer.put(calResult[i]);
            }
        }
        String response = new String(buffer.array());
        Log.d(TAG, "DO calibration response: " + response);
        if (response.contains("calibration")) {
            return "Calibration Successful!";
        }
        else {
            return "Unexpected response from DO probe.\n\nPlease try calibration again!";
        }
    }


    public void setCompensation(String compensation) {
        phService.serviceDrone.uartWrite(compensation.getBytes());
    }

    private String byteToString(byte[] phReading) {
        if (phReading.length == 1) {
            return "Communication Problem";
        }
        // Strip out non ascii charcters
        ByteBuffer buffer = ByteBuffer.allocate(32);
        for (int i = 0; i < phReading.length; i++) {
            if ((int)phReading[i] < 128) {
                buffer.put(phReading[i]);
            }
        }

        return new String(buffer.array());
    }

    public String factoryReset() {
        byte[] response = phService.serviceDrone.uartWriteForRead(FACTORY_RESET, 500);
        if (response.length == 1) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying";
        }
        // Strip out non ascii characters
        ByteBuffer buffer = ByteBuffer.allocate(32);
        for (int i = 0; i < response.length; i++) {
            if ((int)response[i] < 128) {
                buffer.put(response[i]);
            }
        }
        String resetResult = new String(buffer.array());
        Log.d(TAG, "Factory reset response: " + resetResult);
        if (resetResult.contains("reset")) {
            setupProbe();
            return "The probe circuit has been reset to factory defaults";
        }
        else {
            return "Unexpected response from DO probe.\n\nPlease try again!";
        }
    }

    public String setID(String id) {
        String command = "#" + id + "\r";
        byte[] response = phService.serviceDrone.uartWriteForRead(command.getBytes(), 300);
        ByteBuffer buffer = ByteBuffer.allocate(32);
        for (int i = 0; i < response.length; i++) {
            if ((int)response[i] < 128) {
                buffer.put(response[i]);
            }
        }
        String result = new String(buffer.array());
        if (result.contains("Set")) {
            return "The probe ID has been set";
        }
        else {
            return "Unexpected response from pH probe.\n\nPlease try again!";
        }

    }
    public String resetID() {
        byte[] response = phService.serviceDrone.uartWriteForRead(RESET_ID, 100);
        if (response.length == 1) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying";
        }
        // Strip out non ascii characters
        ByteBuffer buffer = ByteBuffer.allocate(32);
        for (int i = 0; i < response.length; i++) {
            if ((int)response[i] < 128) {
                buffer.put(response[i]);
            }
        }
        String result = new String(buffer.array());
        Log.d(TAG, "Reset ID response: " + result);
        if (result.contains("clr")) {
            return "The probe circuit ID has been reset";
        }
        else {
            return "Unexpected response from pH probe.\n\nPlease try again!";
        }

    }

    public String queryID() {
        byte[] response = phService.serviceDrone.uartWriteForRead(QUERY_ID, 100);
        if (response.length == 1) {
            return "There was a problem communication with the probe.\n\nPlease make sure everything is connected, plugged in, and powered on before retrying";
        }
        // Strip out non ascii characters
        ByteBuffer buffer = ByteBuffer.allocate(32);
        for (int i = 0; i < response.length; i++) {
            if ((int)response[i] < 128) {
                buffer.put(response[i]);
            }
        }
        String result = new String(buffer.array());
        Log.d(TAG, "Query ID response: " + result);
        return result;
    }

}
