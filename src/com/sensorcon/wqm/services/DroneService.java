package com.sensorcon.wqm.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;
import com.sensorcon.sensordrone.android.Drone;
import com.sensorcon.sensordrone.android.tools.DroneStreamer;

public class DroneService extends Service implements DroneEventHandler{

    final String TAG = "DroneService";
    public Drone serviceDrone;
    private final IBinder mBinder = new LocalBinder();
    private DroneStreamer ledBlinker;
    private boolean ledToggle = false;

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        if (droneEventObject.matches(DroneEventObject.droneEventType.CONNECTED)) {
            ledBlinker.start();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.CONNECTION_LOST) ||
                droneEventObject.matches(DroneEventObject.droneEventType.DISCONNECTED)) {
            ledBlinker.stop();
        }
    }


    public class LocalBinder extends Binder {
        public DroneService getService() {
            // Return this instance so clients can access the drone
            return DroneService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Service started...");
        serviceDrone = new Drone();
        ledBlinker = new DroneStreamer(serviceDrone, 1000) {
            @Override
            public void repeatableTask() {
                if (ledToggle) {
                    serviceDrone.setLEDs(0, 0, 126);
                }
                else {
                    serviceDrone.setLEDs(0, 0, 0);
                }
                ledToggle = !ledToggle;
            }
        };
        serviceDrone.registerDroneListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(serviceDrone.isConnected) {
            serviceDrone.disconnectNow();
        }
        ledBlinker.stop();
        serviceDrone.unregisterDroneListener(this);
    }
}
