package com.sensorcon.wqm.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.*;
import com.sensorcon.wqm.R;

public class TableRowWithIcon extends TableRow {

    public void setIconResource(int iconResource) {
        this.iconResource = iconResource;
        icon.setImageDrawable(getResources().getDrawable(iconResource));
    }

    public int iconResource;

    public void setDescription(String description) {
        this.description = description;
        display.setText(description);
    }

    public String description;

    public TextView display;
    public ImageView icon;
    public RelativeLayout relativeLayout;

    public TableRowWithIcon(Context context) {
        super(context);
    }

    public TableRowWithIcon(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TableRowWithIcon(Context context, int iconResource, String description) {
        super(context);
        this.iconResource = iconResource;
        this.description = description;


        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.table_row_with_icon, this, true);

        icon = (ImageView)findViewById(R.id.row_icon);
        icon.setImageDrawable(getResources().getDrawable(iconResource));

        display = (TextView)findViewById(R.id.row_description);
        display.setText(description);

        relativeLayout = (RelativeLayout)findViewById(R.id.row_relative_layout);

    }

    public void addToLayout(TableLayout tableLayout) {
        tableLayout.addView(this);
    }

    public void setBgColor(int resourceId) {
        relativeLayout.setBackgroundResource(resourceId);
    }
}
