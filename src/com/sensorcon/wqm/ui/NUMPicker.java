package com.sensorcon.wqm.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.sensorcon.wqm.R;

public class NUMPicker extends LinearLayout {

    public Button btnUp;
    public Button btnDown;
    public TextView displayCharacter;

    private String[] availableCharacters = new String[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"
    };

    private int position;

    public void selectCharacter(int character) {
        this.selectedCharacter = availableCharacters[character];
        this.displayCharacter.setText(selectedCharacter);
        this.position = character;
    }

    public String getSelectedCharacter() {
        return selectedCharacter;
    }

    private String selectedCharacter;

    public NUMPicker(Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.letter_number_picker, this, true);

        position = 0;

        btnUp = (Button)findViewById(R.id.btnUp);
        btnUp.setOnClickListener( new OnClickListener() {
            @Override
            public void onClick(View v) {
                position++;
                if (position == availableCharacters.length) {
                    position = 0;
                }
                selectCharacter(position);
            }
        });
        btnDown = (Button)findViewById(R.id.btnDown);
        btnDown.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                position--;
                if (position < 0) {
                    position = availableCharacters.length - 1;
                }
                selectCharacter(position);
            }
        });
        displayCharacter = (TextView)findViewById(R.id.tvSelectedCharacter);

        selectCharacter(position);

    }

    public NUMPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NUMPicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
