package com.sensorcon.wqm;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.*;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;
import com.sensorcon.wqm.fragments.MainFragment;
import com.sensorcon.wqm.services.DroneService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends FragmentActivity implements DroneEventHandler {

    private TextView tvLoading;
    private SharedPreferences myPreferences;
    private DroneService droneService;
    private boolean mBound;

    public File phLog;
    public FileOutputStream phOutputStream;
    public File doLog;
    public FileOutputStream doOutputStream;

    public static boolean canLog;

    private final String TAG = "WQM:MainActivity";

    public static final boolean DEBUG = false;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first);

        tvLoading = (TextView)findViewById(R.id.main_tv_loading);
        myPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        bindToService();

        // Open our file for logging
        phLog = null;
        doLog = null;
        File rootDir;
        rootDir = Environment.getExternalStorageDirectory();
        if (rootDir.canWrite()) {
            canLog = true;
            // Set up our Directory
            File dir = new File (rootDir.getAbsolutePath() + "/SD_WATER_QUALITY");
            dir.mkdirs();
            // Set up our Files
            phLog = new File(dir,"PH_LOG.csv");
            try {
                phOutputStream = new FileOutputStream(phLog);
                phOutputStream.write("# Unix time (milliseconds), pH Reading\n".getBytes());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            doLog = new File(dir, "DO_LOG.csv");
            try {
                doOutputStream = new FileOutputStream(doLog);
                doOutputStream.write("# Unix time (milliseconds), % DO Saturation, DO Reading (mg/L)\n".getBytes());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            canLog = false;
            MainActivity.genericDialog(this,"Logging Disabled","The app was not able to access your device's external storage. Logging has been disabled!");
        }

    }

    /** Defines callbacks for service binding, passed to bindService() */
    public ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            DroneService.LocalBinder binder = (DroneService.LocalBinder) service;
            droneService = binder.getService();
            mBound = true;
            tvLoading.setVisibility(View.INVISIBLE);
            loadFragment(MainActivity.this, new MainFragment(droneService), false);
            droneService.serviceDrone.registerDroneListener(MainActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            // Take our bat and go home
            if (droneService.serviceDrone.isConnected) {
                droneService.serviceDrone.setLEDs(0, 0, 0);
                droneService.serviceDrone.disconnect();
            }
            MainActivity.this.finish();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mConnection);
        try {
            phOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            doOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        phLog.delete();
        doLog.delete();
    }

    public void bindToService() {
        Intent myIntent = new Intent(getApplicationContext(), DroneService.class);
        bindService(myIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    public static void loadFragment(FragmentActivity activity, Fragment fragment, Boolean addToBackstatck) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragment);
        if (addToBackstatck) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.commit();
    }

    public static void popMyBackStach(FragmentActivity activity) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager.popBackStack();
    }

    public static void genericDialog(Context context, String title, String msg) {
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setIcon(R.drawable.ic_launcher);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    public static void uiToast(final FragmentActivity mainActivity, final String msg) {
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mainActivity, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        if (droneEventObject.matches(DroneEventObject.droneEventType.CONNECTION_LOST)) {
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentManager.popBackStack();
            MainActivity.loadFragment(this, new MainFragment(droneService), false);
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String msg = "It seems the connection to the Sensordrone was lost!";
                    msg += "\n\nPerhaps you have moved out of range, or your Sensordrones battery is low.";
                    msg += "\n\nIf you can't re-connect, you can still export any logged data for this session from the 'App Settings'";
                    MainActivity.genericDialog(MainActivity.this, "Connection Lost!", msg);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onPause");

    }



}
